if (!window.console) console = {log: function() {}};
window.log=function(){log.history=log.history||[];log.history.push(arguments);if(this.console){console.log(Array.prototype.slice.call(arguments))}};

FoeMap.Layer = function(options)
{   var name = options.name || "<Unnamed Layer>";
	var parent_map = options.parent_map;
	var datasets = options.datasets || [];
	var markers = [];
	var visible = options.visible || false;
	var icons = options.icons || ["/maps/static/rendered_maps/img/markers/default.png"];
	var legend_icon = options.legend_icon || icons[0];
	var z_index = options.z_index || 0;
	var cluster = options.cluster;
	var cluster_icon = options.cluster_icon || "/maps/static/rendered_maps/img/markers/cluster.png";
	var cluster_text_color = '#' + options.cluster_text_color || 'white';
	var cluster_grid_size = options.cluster_grid_size || 50;  // todo add to model
	var cluster_max_zoom = options.cluster_max_zoom || 8;  // todo add to model
	if(cluster)
	{   var clusterer_options = {gridSize: cluster_grid_size, maxZoom: cluster_max_zoom, styles: [ { textColor: cluster_text_color, url: cluster_icon, height: 37, width: 32}] };
		var marker_clusterer = new MarkerClusterer(parent_map.getMap(), null, clusterer_options);
	}

	this.getName = function(){ return name };
	this.getParentMap = function(){ return parent_map };
	this.getDataset = function(dataset_name){ return _.findWhere(datasets, {name: dataset_name}); };
	this.getIcon = function(){ return icons[_.random(icons.length-1)] };
	this.getLegendIcon = function(){ return legend_icon };
	this.getZIndex = function(){ return z_index };
	this.isVisible = function(){ return visible };
	this.setVisibility = function(new_visibility){ visible = new_visibility };
	this.update = function(){ update() };

	// Make sure we only call refreshMarkers once after all local rendered datasets have their markers
	this.refreshMarkers = null;

	this.addDataset = function(dataset)
	{   datasets.push(dataset);
		var local_dataset_count = _.size(_.filter(datasets, function(dataset){ return dataset.getRenderLocation() == 'local' }));
		this.refreshMarkers = _.after(local_dataset_count, function()
		{   markers = _.flatten(_.invoke(datasets, 'getMarkers'));
			console.log(name + ': ' + markers.length);
			update();
		}, this);
	};

	this.refreshRemoteMarkerLayer = function()
	{   markers = _.invoke(datasets, 'getMarkers');
		update();
	}

	function update()
	{   _.each(datasets, function(dataset) { dataset.fetchMarkers(); });

		if(!_.isEmpty(markers))
			visible ? showMarkers() : hideMarkers();
	}

	function showMarkers()
	{   if(cluster)
			marker_clusterer.addMarkers(markers);
		else
			_.each(markers, function(marker)
			{   try { marker.setMap(parent_map.getMap()); }
				catch(e) { console.log('Could not set map for marker ' + marker + ' on layer ' + name); }
			});
	}

	function hideMarkers()
	{   if(cluster)
			marker_clusterer.clearMarkers();
		else
			_.each(markers, function(marker)
			{   try { marker.setMap(null); }
				catch(e) { console.log('Could not unset map for marker ' + marker + ' on layer ' + name); }
			});
	}
};



FoeMap.Dataset = function(options)
{	this.name = options.name || "<Unnamed Dataset>";
	var parent_layer = options.parent_layer;
	var source_location = options.source_location;
	var source_type = options.source_type;
	var render_location = options.render_location || 'local';
	var fusion_styles = options.fusion_styles;
	var fields = options.fields || { lat: 'lat', lng: 'lng'};
	var fields_array = _.values(fields);
	var popup_html = options.popup_html || null;
	var parser = options.parser || function(json) { return json; }
	var max_marker_count = options.max_marker_count;
	var markers = [];
	var selected_pin = null;

	if(popup_html)
	{   // TODO Replace with underscore templates
		var popup_template = Handlebars.compile(popup_html);
		var popup_fields = popup_html.match(/\{\{([^}]+)\}\}/g) || []; // capturing, but how to use with g flag?
		popup_fields = _.map(popup_fields, function(field){ return field.match(/\w+/)[0]; });
		popup_fields = _.without(popup_fields, 'if', 'else');  // TODO: remove filter, push detection to regexp, and allow fields called 'if'
		fields_array = fields_array.concat(popup_fields);
	}

	var fusionApiUri = options.fusionApiUri || "https://www.googleapis.com/fusiontables/v1/";
	var fusionApiKey = options.fusionApiKey || "AIzaSyCpG6we9vLNRv6x6dK12aCuEFRBUiczDUE";

	this.getMarkers = function(){ return markers; };
	this.getRenderLocation = function(){ return render_location; }
	this.fetchMarkers = _.once(fetchMarkers);

    this.addPin = function(pinData, zoomTo)
    {   var marker = createMarker(pinData.lat, pinData.lng, parent_layer.getIcon(), parent_layer.zIndex);
        if(_.size(pinData) >= 2)    // Infer we have more data than just lat and lng, so create a popup
	        addPopup(marker, pinData);
	    markers.push(marker);
        parent_layer.refreshMarkers();
	    if(zoomTo == true)
	    {   parent_layer.getParentMap().getMap().setZoom(10);
		    parent_layer.getParentMap().getMap().setCenter(new google.maps.LatLng(pinData.lat, pinData.lng));
		    google.maps.event.trigger(marker, 'click');
	    }
    }


	function fetchMarkers()
	{   selected_pin = _.pick(parent_layer.getParentMap().getSelectedPin(), 'lat', 'lng', 'uid', 'zoom');

		if(render_location == 'local' && source_type == 'xml')
			$.ajax({ type: "GET", dataType: "xml", url: source_location, success: function(data){ processFeed(data, source_type); }});

		else if(render_location == 'local' && source_type == 'json')
			$.ajax({ type: "GET", dataType: "json", url: source_location, success: function(data){ processFeed(data, source_type); }});

		else if(render_location == 'local' && source_type == 'fusion')
		{   var sql = 'select ' + fields_array.join() + ' from ' + source_location;
			var url = fusionApiUri + "query?key=" + fusionApiKey + "&sql=" + sql;
			$.ajax({ type: "GET", dataType: "jsonp", url: url, success: function(response){ processFeed(response, source_type); }});
		}

		else if(render_location == 'remote' && source_type == 'xml')
		{	markers = new google.maps.KmlLayer(source_location, { preserveViewport: true });
			parent_layer.refreshRemoteMarkerLayer();
		}

		else if(render_location == 'remote' && source_type == 'fusion')
		{   var fusion_params = { query: { select: fields_array.join(), from: source_location } }
			if(fusion_styles && fusion_styles.templateId)
				fusion_params.templateId = fusion_styles.templateId
			if(fusion_styles && fusion_styles.styleId)
					fusion_params.styleId = fusion_styles.styleId
			markers = new google.maps.FusionTablesLayer(fusion_params);
			parent_layer.refreshRemoteMarkerLayer();
		}
		else
			console.log('Render location "' + render_location + '" with "' + source_type + '" source type not supported. Can\'t fetch markers.');
	}

	function processFeed(data, source_type)
	{   if (source_type == 'xml')
			data = $.xml2json(data);

		if (source_type == 'xml' || source_type == 'json')
			data = parser(data);

		else if (source_type == 'fusion')
		{   if(data.error)
			{   // TODO: if location path end with /preview/ then alert else console log
				console.log('Could not render fusion table: ' + data.error.message);
			}
			data = data.rows;
		}

		if(max_marker_count && _.size(data) > max_marker_count)
		{	data = _.first(data, max_marker_count);
			console.log('Truncating. Max marker set to ' + max_marker_count);
		}

		_.each(data, function(markerData, index, list)
		{   if (_.isArray(markerData))
				var marker = createMarker(markerData[0], markerData[1], markerData);
			else
				var marker = createMarker(markerData[fields['lat']], markerData[fields['lng']], markerData);
			markers.push(marker);
		}, this);

		parent_layer.refreshMarkers();
	}

	function createMarker(lat, lng, markerData, uid)
	{   var marker = new google.maps.Marker( { position: new google.maps.LatLng(lat, lng), z_index: parent_layer.getZIndex() } );
		marker.lat = lat;
		marker.lng = lng;
		marker.setIcon(parent_layer.getIcon());
		addPopup(marker, markerData);

		// Embedded check for pin
		if((selected_pin.lat == lat && selected_pin.lng == lng) || (markerData.uid != null && selected_pin.uid != null && selected_pin.uid == markerData.uid))
		{   parent_layer.getParentMap().setSelectedPin({lat: marker.lat, lng: marker.lng, uid: markerData.uid, marker: marker});
			parent_layer.getParentMap().showSelectedPin();
		}

		return marker;
	}

	function addPopup(marker, markerData)
	{   if(popup_html)
		{   google.maps.event.addListener(marker, "click", function ()
			{   var infoWindow = parent_layer.getParentMap().getInfoWindow();
				if (_.isArray(markerData))
					markerData = _.object(fields_array, markerData);
				infoWindow.setContent('<div class="foeInfoWindow">' + popup_template(markerData) + '</div>');
				infoWindow.open(parent_layer.getParentMap().getMap(), this);
				parent_layer.getParentMap().setSelectedPin(marker);
				parent_layer.getParentMap().trackEvent('Show Marker Popup');
			});
		}
	}
};


function FoeMap(options)
{
	if(!options){ var options = {}; }
	var name = options.name || "<Unamed Map>";
	var layers = [];
	var map_element_id = options.map_element_id || "map";
	var center_lat_lng = options.center_lat_lng || new google.maps.LatLng(54.7, -2.0);
	var initial_zoom = options.initial_zoom || 6;
	var tile_set = options.tile_set || google.maps.MapTypeId.TERRAIN;  // mapTypeId: google.maps.MapTypeId.ROADMAP,
	var legend_container_id = options.legend_container_id || "legend";
	var shareable_url_root = options.shareable_url_root || location.protocol + '//' + location.host + location.pathname;
	var infoWindow = new google.maps.InfoWindow();
	var accept_submissions = options.accept_submissions || false;
	var submissions_layer_name = options.submissions_layer_name || null;
	var selected_pin = {};

	var gmap = new google.maps.Map(document.getElementById(map_element_id), {
		center: center_lat_lng,
		zoom: initial_zoom,
		mapTypeId: tile_set,
		zoomControl : true,
		zoomControlOptions: { style : google.maps.ZoomControlStyle.SMALL, position: google.maps.ControlPosition.TOP_LEFT },
		panControl : false,
		streetViewControl : false,
		mapTypeControl: false,
		overviewMapControl: false
	});

	this.getMap = function(){ return gmap };
	this.setLayers = function(new_layers){ layers = new_layers };
	this.addLayer = function(layer){ layers.push(layer) };
	this.getLayer = function(layer_name){ return _.reduce(layers, function(memo, layer){ if(layer.getName() == layer_name) return layer; }, null); };
	this.getInfoWindow = function(){ return infoWindow };
	this.getSelectedPin = function(){ return selected_pin };
	this.trackEvent = function(value){ trackEvent(); };
	this.minimizeLegend = function(){ minimizeLegend() };
	this.maximizeLegend = function(){ maximizeLegend() };

	this.render = function()
	{   parseSharedUrl();
		_.each(layers, function(layer){ layer.update(); });     // Update all layers;
		addLayersToLegend($('#' + legend_container_id));
		userLocateControl($('#' + legend_container_id), this.codeAddress);
		userShareButton();
		gmap.controls[google.maps.ControlPosition.RIGHT_TOP].push(document.getElementById(legend_container_id));

		if(accept_submissions)
		{   $('#' + legend_container_id).append($('#add_your_pin_container'));
			$.cookie.json = true;
			reviveSubmissionCookie();
		}
	}

	function addLayersToLegend(container)
	{   _.each(layers, function(layer)
		{   var label = $("<label>").html('<span>' + layer.getName() + '</span>')
			var icon = $("<img>").attr('src',layer.getLegendIcon());
			var input = $('<input/>', { name: layer.getName(), type: 'checkbox', value: layer.name, checked: layer.isVisible() })
				.change(function ()
				{   layer.setVisibility(this.checked);
					layer.update();
					trackEvent('Layer Toggle - ' + this.value + ':' + this.checked);
				});
			label.prepend(icon).prepend(input);
			container.append(label);
		});
	}

	// User locate, postcode or geolocation API if browser supported
	function userLocateControl(container, geocodeMethod)
	{   container.append($('#postcode_locator')); // Append postcode search to legend

		var locateCallback = function (location)
		{   if (location.coords)
			{   var location = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
			}
			new google.maps.Marker({ map: gmap, position: location });
			gmap.setCenter(location);
			gmap.setZoom(13);
		}

		$('#postcode_locator').submit(function()
		{   var address = document.getElementById('postcode_input').value;
			geocodeMethod(address, locateCallback);
			trackEvent('Postcode Search');
			return false;
		});

		if (navigator.geolocation)
		{   $('#geolocate_feature').show();
			$('#geolocate_button').click(function()
			{   navigator.geolocation.getCurrentPosition(locateCallback);
				trackEvent('Geolocate');
				return false;
			});
		}

		if($('#postcode_input').val() != '')
		{   $('#postcode_input').submit();
		}
	}

	// Geocoding
	this.codeAddress = function(address, callback, callback_opts)
	{   var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { 'address': address, 'region': 'uk'}, function(results, status)
		{   if (status == google.maps.GeocoderStatus.OK)
				callback(results[0].geometry.location, callback_opts);
			else
				console.log('Geocoding failed for address ' + address +': ' + status);
				// TODO pass status to callback, have map handle failure state e.g. "sorry couldn't find that postcode" message
		});
	}


	// Sharing URL
	var urlParams = {};

	function getSharableUrl()
	{   urlParams =
		{   layers: _.invoke(_.filter(layers, function(layer){ return layer.isVisible() }), 'getName'),
			zoom: gmap.getZoom(),
			lat: gmap.getCenter().lat(),
			lng: gmap.getCenter().lng(),
			selectedPin: _.omit(selected_pin, 'marker', 'zoom')
		};

		if($('#postcode_input').val())
			urlParams.mapspostcode = $('#postcode_input').val();

		var url = shareable_url_root + '?' + jQuery.param(urlParams);  // TODO Switch to display in textbox, pre-selected
		$('#share_url').val(url).focus().select();
	};

	function parseSharedUrl()
	{   var params = $.parseParams( window.location.href.split('?')[1] || '' );

		if(params['layers[]'])
		{   _.each(layers, function(layer)
			{   if(_.isString(params['layers[]']))
					params['layers[]'] = [params['layers[]']];
				if(_.contains(params['layers[]'], layer.getName()))
					layer.setVisibility(true);
				else
					layer.setVisibility(false);
			});
		}

		if (_.has(params, "lat") && _.has(params, "lng"))
			gmap.setCenter(new google.maps.LatLng(params.lat, params.lng));

		if (_.has(params, "mapspostcode"))
			$('#postcode_input').val(params.mapspostcode).submit();

		if (_.has(params, "selectedPin[lat]") && _.has(params, "selectedPin[lng]"))
		{   selected_pin.lat = params['selectedPin[lat]'];
			selected_pin.lng = params['selectedPin[lng]'];
		}

		if (_.has(params, "selectedPin[uid]"))
			selected_pin.uid = params['selectedPin[uid]'];

		if (_.has(params, "zoom"))
		{	selected_pin.zoom = parseFloat(params['zoom']);
			gmap.setZoom(parseFloat(params['zoom']));
		}
	};


	this.setSelectedPin = function(pinData)
	{   if(pinData.uid)
			selected_pin.uid = pinData.uid;

		if(pinData.marker)
			selected_pin.marker = pinData.marker;

		if(!isNaN(pinData.lat) && !isNaN(pinData.lng))
		{   selected_pin.lat = pinData.lat;
			selected_pin.lng = pinData.lng;
		}
	};


	this.showSelectedPin = function()
	{   gmap.setCenter(new google.maps.LatLng(selected_pin.lat, selected_pin.lng));
		if(selected_pin.zoom)
			gmap.setZoom(selected_pin.zoom);
		else
			gmap.setZoom(10);
		google.maps.event.trigger(selected_pin.marker, 'click');
	};


	function userShareButton()
	{   $('#share_button').click(function() { getSharableUrl(); });
	}

	function trackEvent(value)
	{   try
		{   var track_array = ['_trackEvent', 'General Action', 'Map: ' + name, value ];
			_gaq.push(track_array);
			// console.log(track_array.join(' '));
		}
		catch (e){ console.log("Google Analytics not found.  Could not track event.") }
	}

	$('#minimize_button').click(function()
	{   $('#legend').data('toggle') == 'max' ? minimizeLegend() : maximizeLegend();
	});

	function minimizeLegend()
	{   $('#legend').css('height','23px').css('width','14px');
		$('#legend #minimize_button i').removeClass('icon-resize-small').addClass('icon-resize-full');;
		$('#legend label').css('visibility', 'hidden');
		$('#legend').data('toggle', 'min');
	}

	function maximizeLegend()
	{	$('#legend').css('height','auto').css('width','auto');
		$('#legend label').css('visibility', 'visible');
		$('#legend #minimize_button i').removeClass('icon-resize-full').addClass('icon-resize-small');;
		$('#legend').data('toggle', 'max');
	}


	// User Submissions
	if(accept_submissions)
	{   $('#add_your_pin_button').click(function()
		{   foeMap.minimizeLegend();
		    $('#add_your_pin_form').show();
		    foeMap.trackEvent('Add Pin Button Clicked');
		});

		$('.closeForm').click(function()
		{   $('#add_your_pin_form').hide();
		    $('#form_body').css('visibility', 'visible');
		    $('#form_confirmation').css('visibility', 'hidden');
		    foeMap.maximizeLegend();
		});

		$('#id_postcode').focusout(function()   // Hack in a quick geocode on postcode blur
		{   foeMap.codeAddress($('#id_postcode').val(), function(location)
		    {   console.log('coded: ' + location);
		        $('#id_latitude').val(location.lat());
		        $('#id_longitude').val(location.lng());
		    });
		});

		$('#add_your_pin_form').iframePostForm(
		{   json: true,
		    complete: function (response)
		    {   if(response['id'])
		            submissionSuccess(response);
		        else
		            console.log('Unexpected response from form submission: ' + response);
		    }
		});

		function submissionSuccess(response)
		{   console.log('Submission successful.  Id: ' + response['id']);
		    $('#form_body').css('visibility', 'hidden');  /* Visibility, not display, to maintain height */
		    $('#form_confirmation').css('visibility', 'visible');
		    createSubmissionCookie($('#id_latitude').val(), $('#id_longitude').val(), response['photo'], response['description']);
		    if($.cookie(name + '_submission_location'))
		        foeMap.getLayer(submissions_layer_name).getDataset(submissions_layer_name).addPin($.cookie(name + '_submission_location'), true);
		    $('#add_your_pin_form')[0].reset();
		    foeMap.trackEvent('Add Pin Submitted | Id: ' + response['id']);
		}

		function createSubmissionCookie(lat, lng, photo, desc)
		{   $.cookie(name + '_submission_location', {'lat': lat, 'lng':lng, 'photo': photo,'desc': desc }, { expires: 30, path: '/' });
		}

		function reviveSubmissionCookie()
		{   if($.cookie(name + '_submission_location'))
		        foeMap.getLayer(submissions_layer_name).getDataset(submissions_layer_name).addPin($.cookie(name + '_submission_location'));
		}

		// Submit form validation
		$.validator.addMethod('ukpostcode', function(value, element)
		{   return this.optional(element) || /(^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$)|(^xxx xxx$)/.test(value);
		}, "Enter UK Postcode (ie, N1 7JQ)");

		var validator = $('#add_your_pin_form').validate(
		{   rules:
		    {   firstname: 'required',
		        lastname: 'required',
		        email: { required: true, email: true },
		        postcode: { required: true, ukpostcode: true },
		        description: 'required'
		    },
		    errorPlacement: function(error, element){ return; }
		});
	}
}


// jquery.parseparams.js
// https://gist.github.com/kares/956897#comment-802666

(function ($)
{   var re = /([^&=]+)=?([^&]*)/g;
	var decode = function (str){ return decodeURIComponent(str.replace(/\+/g, ' ')); };

	$.parseParams = function (query)
	{	var params = {}, e;
		if(query)
		{	if(query.substr(0, 1) == '?')
				query = query.substr(1);

			while(e = re.exec(query))
			{	var k = decode(e[1]);
				var v = decode(e[2]);
				if(params[k] !== undefined)
				{	if(!$.isArray(params[k]))
						params[k] = [params[k]];
					params[k].push(v);
				}
				else
					params[k] = v;
			}
		}
		return params;
	};
})(jQuery);