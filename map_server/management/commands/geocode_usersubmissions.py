# coding=UTF-8

import logging
log = logging.getLogger(__name__)

from django.core.management.base import BaseCommand
from map_server.utils import geocodePostcode
from map_server.models import UserSubmission

from django.conf import settings
from django.db.models import Q


class Command(BaseCommand):
    help = 'Geocode user submissions'

    def handle(self, *args, **options):
        submissions_to_geocode = UserSubmission.objects.filter(Q(latitude__isnull=True) | Q(longitude__isnull=True))\
            .exclude(geocoding_attempts__gte=settings.GEOCODE_MAX_ATTEMPTS)\
            .exclude(postcode='')
        for submission in submissions_to_geocode:
            try:
                geocode_result = geocodePostcode(submission.postcode)
                submission.lat = geocode_result['lat']
                submission.lng = geocode_result['lng']
            except KeyError:
                pass
            except Exception as e:
                log.error("User submission geocoding failed: %s" % e)
            submission.geocoding_attempts += 1
            submission.save()