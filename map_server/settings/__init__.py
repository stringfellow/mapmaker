# coding=utf-8

# From: https://code.djangoproject.com/wiki/SplitSettings#SettingInheritancewithHierarchy

import os

merge_keys = ('INSTALLED_APPS', 'MIDDLEWARE_CLASSES', 'TEMPLATE_CONTEXT_PROCESSORS')  # Merge instead of copy these keys


def deep_update(from_dict, to_dict):
    for (key, value) in from_dict.iteritems():
        if key in to_dict.keys() and isinstance(to_dict[key], dict) and isinstance(value, dict):
            deep_update(value, to_dict[key])
        elif key in merge_keys:
            if not key in to_dict:
                to_dict[key] = ()
            to_dict[key] = to_dict[key] + from_dict[key]
        else:
            to_dict[key] = value


modules = ('common', 'active')
current = __name__

for module_name in modules:
    try:
        module = getattr(__import__(current, globals(), locals(), [module_name]), module_name)
    except ImportError, e:
        print 'ERROR: Unable to import %s configuration: %s' % (module_name, e)
        raise
    except AttributeError, e:
        raise

    # create a local copy of this module's settings
    module_settings = {}
    for setting in dir(module):
        # all django settings are uppercase, so this ensures we
        # are only processing settings from the dir() call
        if setting == setting.upper():
            module_settings[setting] = getattr(module, setting)
    deep_update(module_settings, locals())

#print locals() # for debugging


# PyCharm collectstatic workaround
try: INSTALLED_APPS
except NameError: INSTALLED_APPS += ('django.contrib.staticfiles', 'south', 'django_extensions',)
