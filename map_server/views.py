# coding=UTF-8

import logging

log = logging.getLogger(__name__)

import csv
import datetime
from django.http import HttpResponse
from django.views.generic import DetailView
from django.views.generic.edit import BaseUpdateView, FormView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from map_server.models import UserSubmission, Map, Layer, LayerDataset, MarkerIcon
from map_server.serializers import *
from map_server.permissions import CreateOrAdminOnly
from map_server.renderers import PlainTextJSONRenderer
from map_server.forms import ConvertEnToLatlngForm, BulkGeocodeForm
from map_server.utils.transform import *
from map_server.utils import *


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class UserSubmissionList(generics.ListCreateAPIView):
    model = UserSubmission
    serializer_class = UserSubmissionSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_template_names(self):
        return ['moderation/list.html']

    def get_queryset(self):
        try:
            if 'map_id' in self.kwargs.keys():
                map = Map.objects.get(id=self.kwargs['map_id'])
            else:
                map = Map.objects.all()[0]

            if self.request.user and self.request.user.is_staff:
                return UserSubmission.objects.filter(map=map)
            return UserSubmission.objects.filter(approval_status='accepted', latitude__isnull=False,
                                                 longitude__isnull=False, map=map)
        except:
            log.error('No maps defined yet')
            return None

    def get(self, request, *args, **kwargs):
        if request.accepted_renderer.format == 'html':
            try:
                if 'map_id' in self.kwargs.keys():
                    map = Map.objects.get(id=self.kwargs['map_id'])
                else:
                    map = Map.objects.all()[0]
            except IndexError:
                map = None
                messages.info(request, 'No maps created yet')

            try:
                object_list = self.get_queryset()
                context = self.get_context_data(object_list=object_list,
                                                pending_list=object_list.filter(approval_status='pending'),
                                                accepted_list=object_list.filter(approval_status='accepted'),
                                                rejected_list=object_list.filter(approval_status='rejected'),
                                                map_list=Map.objects.all(),
                                                selected_map=map)
                return Response(context)
            except AttributeError:
                log.debug('No maps exist yet')
                return Response()
        return super(UserSubmissionList, self).get(request, *args, **kwargs)


# Work around needed with special serializer because IE is _crap_
class UserSubmissionListPost(UserSubmissionList):
    serializer_class = UserSubmissionSerializer
    renderer_classes = (PlainTextJSONRenderer,)
    permission_classes = (CreateOrAdminOnly,)


class UserSubmissionListPublic(UserSubmissionList):
    serializer_class = UserSubmissionPublicSerializer
    renderer_classes = (PlainTextJSONRenderer,)
    permission_classes = (IsAuthenticatedOrReadOnly,)


class UserSubmissionDetail(generics.RetrieveUpdateDestroyAPIView):
    model = UserSubmission
    serializer_class = UserSubmissionSerializer
    permission_classes = (CreateOrAdminOnly,)

    def get_template_names(self):
        return ['moderation/detail.html']


@login_required()
def user_submission_email_addresses(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="user_submission_%s.csv"' % \
                                      datetime.datetime.now().strftime("%d-%m-%Y")
    response = UserSubmission.write_data_as_csv(response)
    return response


class ConvertEnToLatLng(LoginRequiredMixin, FormView):
    template_name = 'utils/ConvertEnToLatLng.html'
    form_class = ConvertEnToLatlngForm

    def form_valid(self, form):
        f = form.files['file_to_convert']
        data = [row for row in csv.reader(f.read().splitlines())]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="en_to_latlng_converted_%s.csv"' % \
                                          datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")
        writer = csv.writer(response)

        for row in data:
            easting, northing, lat, lng = '-', '-', '-', '-'
            try:
                easting, northing = row[0], row[1]
                converted = british_grid_to_wgs(easting, northing)
                lat, lng = converted['lat'], converted['lng']
            except Exception as e:
                print 'Could not convert en row: %s, %s' % (easting, northing)
                log.debug('Could not convert en row: %s, %s' % (easting, northing))
            writer.writerow([easting, northing, lat, lng])
        return response


class BulkGeocode(LoginRequiredMixin, FormView):
    template_name = 'utils/BulkGeocodeForm.html'
    form_class = BulkGeocodeForm

    def form_valid(self, form):
        f = form.files['file_to_convert']
        data = [row for row in csv.reader(f.read().splitlines())]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="geocoded-%s.csv"' % \
                                          datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")
        writer = csv.writer(response)

        for row in data:
            postcode, lat, lng = '-', '-', '-'
            try:
                postcode = row[0]
                geocoded = geocodePostcode(postcode)
                lat, lng = geocoded['lat'], geocoded['lng']
            except Exception as e:
                log.debug('Could not geocode postcode: %s' % postcode)
            writer.writerow([postcode, lat, lng])
        return response


class MapPreview(LoginRequiredMixin, DetailView):
    model = Map

    def render_to_response(self, context, **response_kwargs):
        return self.get_object().preview()


class MapPublish(LoginRequiredMixin, BaseUpdateView):
    model = Map

    def post(self, request, *args, **kwargs):
        self.object = self.get_object().write_to_disk()
        return HttpResponse(status=201)

    def render_to_response(self, context, **response_kwargs):
        return HttpResponse(status=200)


class MapList(generics.ListCreateAPIView):
    model = Map
    serializer_class = MapSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class MapDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Map
    serializer_class = MapSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class LayerList(generics.ListCreateAPIView):
    model = Layer
    serializer_class = LayerSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class LayerDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Layer
    serializer_class = LayerSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class LayerDatasetList(generics.ListCreateAPIView):
    model = LayerDataset
    serializer_class = LayerDatasetSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class LayerDatasetDetail(generics.RetrieveUpdateDestroyAPIView):
    model = LayerDataset
    serializer_class = LayerDatasetSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class MarkerIconList(generics.ListCreateAPIView):
    model = MarkerIcon
    serializer_class = MarkerIconSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)


class MarkerIconDetail(generics.RetrieveUpdateDestroyAPIView):
    model = MarkerIcon
    serializer_class = MarkerIconSerializer
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)