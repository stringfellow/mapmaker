Development Setup
=================

Prerequisites
-------------
Python 2.7
Virtualenv


Checkout Repo
-------------
`git clone https://bitbucket.org/foeewni/mapmaker.git`

`cd checkout_dir`
`virtualenv env`
`. env/bin/activate`

# Remove post_office's migrations
`rm -r env/lib/python2.7/site-packages/post_office/migrations`

# Syncdb and South migrate
`maps/manage.py syncdb; maps/manage.py migrate map_server`

# Load data from fixtures
`maps/manage.py runscript map_server.fixtures.maps`
`maps/manage.py loaddata exclusion_boxes`

# Add cronjobs


Other Notes
-----------
If pip install throws an error on OS X 10.9.2:
`ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install pyproj==1.9.3 lxml==3.2.1`
then complete environment install as usual `pip install -r requirements.txt`


Roadmap
-------
- Add fixture for generic admin and moderator accounts
- Dev provisioned with Ansible and Vagrant
- Switch to settings and settings_local
- Remove emailing backend
- Update components
- Move mapit to proper source
- Remove horrible URL_PREFIX hack


Debian/Ubuntu Install
=====================
`sudo apt-get install python-dev python-pip python-virtualenv libxml2-devel libxslt-devel`
